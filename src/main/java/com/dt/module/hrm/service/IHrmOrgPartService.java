package com.dt.module.hrm.service;

import com.dt.module.hrm.entity.HrmOrgPart;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-04-13
 */
public interface IHrmOrgPartService extends IService<HrmOrgPart> {

}
