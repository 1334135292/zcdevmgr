package com.dt.module.cmdb.service;

import com.dt.module.cmdb.entity.ResAttrs;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-06-18
 */
public interface IResAttrsService extends IService<ResAttrs> {

}
