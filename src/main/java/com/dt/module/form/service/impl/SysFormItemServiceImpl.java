package com.dt.module.form.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.module.form.entity.SysFormItem;
import com.dt.module.form.mapper.SysFormItemMapper;
import com.dt.module.form.service.ISysFormItemService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2020-03-28
 */
@Service
public class SysFormItemServiceImpl extends ServiceImpl<SysFormItemMapper, SysFormItem> implements ISysFormItemService {

}
