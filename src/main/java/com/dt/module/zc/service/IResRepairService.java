package com.dt.module.zc.service;

import com.dt.module.zc.entity.ResRepair;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-04-19
 */
public interface IResRepairService extends IService<ResRepair> {

}
