package com.dt.module.zc.service;

import com.dt.module.zc.entity.ResInoutItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-06-27
 */
public interface IResInoutItemService extends IService<ResInoutItem> {

}
