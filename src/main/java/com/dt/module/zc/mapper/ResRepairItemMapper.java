package com.dt.module.zc.mapper;

import com.dt.module.zc.entity.ResRepairItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-04-25
 */
public interface ResRepairItemMapper extends BaseMapper<ResRepairItem> {

}
