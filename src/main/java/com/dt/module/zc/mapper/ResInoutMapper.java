package com.dt.module.zc.mapper;

import com.dt.module.zc.entity.ResInout;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-05-27
 */
public interface ResInoutMapper extends BaseMapper<ResInout> {

}
