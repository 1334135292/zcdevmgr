package com.dt.module.flow.mapper;

import com.dt.module.flow.entity.SysProcessSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-06-26
 */
public interface SysProcessSettingMapper extends BaseMapper<SysProcessSetting> {

}
