package com.dt.module.flow.mapper;

import com.dt.module.flow.entity.SysProcessForm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-04-10
 */
public interface SysProcessFormMapper extends BaseMapper<SysProcessForm> {

}
