package com.dt.module.flow.service;

import com.dt.module.flow.entity.SysProcessForm;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author algernonking
 * @since 2020-04-10
 */
public interface ISysProcessFormService extends IService<SysProcessForm> {

}
