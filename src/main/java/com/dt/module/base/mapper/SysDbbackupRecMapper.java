package com.dt.module.base.mapper;

import com.dt.module.base.entity.SysDbbackupRec;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-06-25
 */
public interface SysDbbackupRecMapper extends BaseMapper<SysDbbackupRec> {

}
