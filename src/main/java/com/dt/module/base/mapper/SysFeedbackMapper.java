package com.dt.module.base.mapper;

import com.dt.module.base.entity.SysFeedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author algernonking
 * @since 2020-05-16
 */
public interface SysFeedbackMapper extends BaseMapper<SysFeedback> {

}
