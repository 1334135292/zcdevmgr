package com.dt.module.base.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dt.module.base.entity.SysLogAccess;
import com.dt.module.base.mapper.SysLogAccessMapper;
import com.dt.module.base.service.ISysLogAccessService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author algernonking
 * @since 2018-07-23
 */
@Service
public class SysLogAccessServiceImpl extends ServiceImpl<SysLogAccessMapper, SysLogAccess> implements ISysLogAccessService {

}
